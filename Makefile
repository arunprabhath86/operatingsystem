GCCPARAMS= -m32 -Iinclude  -nostdlib -nostdinc -fno-builtin -fno-stack-protector -Wno-write-strings \
			-Wincompatible-pointer-types
ASPARAMS= --32
LINKPARAMS= -melf_i386

objects = obj/loader.o \
			obj/kernel.o \
			obj/gdt.o \
			obj/interrupt/isr.o\
			obj/gdt_assembly.o \
			obj/hardware/pci.o \
			obj/driver/vga.o \
			obj/driver/display.o \
			obj/driver/keyboard.o \
			obj/driver/mouse.o \
			obj/driver/timer.o \
			obj/util/util.o \
			obj/interrupt/interruptstub.o \
			obj/interrupt/interrupt.o \
			

obj/%.o: src/%.c
	mkdir -p $(@D)
	gcc $(GCCPARAMS) -o $@ -c $<
obj/%.o: src/%.s
	mkdir -p $(@D)
	as $(ASPARAMS) -o $@ $<
kernel.bin: linker.ld $(objects) 
	ld $(LINKPARAMS) -T $< -o $@ $(objects)

kernel.iso: kernel.bin
	mkdir iso
	mkdir iso/boot
	mkdir iso/boot/grub
	cp kernel.bin iso/boot/kernel.bin
	echo 'set timeout=0'                      > iso/boot/grub/grub.cfg
	echo 'set default=0'                     >> iso/boot/grub/grub.cfg
	echo ''                                  >> iso/boot/grub/grub.cfg
	echo 'menuentry "My Operating System" {' >> iso/boot/grub/grub.cfg
	echo '  multiboot /boot/kernel.bin'      >> iso/boot/grub/grub.cfg
	echo '  boot'                            >> iso/boot/grub/grub.cfg
	echo '}'                                 >> iso/boot/grub/grub.cfg
	grub-mkrescue --output=kernel.iso iso
	rm -rf iso 
.PHONY: clean
clean:
	rm -rf obj kernel.bin kernel.iso

