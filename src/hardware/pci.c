#include <hardware/pci.h>
#include <hardware/port.h>
#include <driver/display.h>


uint32_t construct_device_id(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset);

uint32_t pci_read(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset) {
    uint32_t id = construct_device_id(bus, device, function, register_offset);
    w32_port(PCI_COMMAND_PORT, id);
    uint32_t result = r32_port(PCI_DATA_PORT);
    return result >> (8* (register_offset % 4));
}
void pci_write(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset, uint32_t data){
    uint32_t id = construct_device_id(bus, device, function, register_offset);
    w32_port(PCI_COMMAND_PORT, id);
    w32_port(PCI_DATA_PORT, data);
}


uint32_t construct_device_id(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset) {
    uint32_t id = 0x1 << 31 |
                ((bus & 0xFF) << 16) |
                ((device & 0x1F) << 11) |
                ((function & 0x07) << 7) |
                (register_offset & 0xFC);
    return id;
}

int8_t device_has_functions(uint16_t bus, uint16_t device) {
    return pci_read(bus, device, 0, 0x0E) & (1 << 7);
}

pci_device_descriptor get_device_descriptor(uint16_t bus, uint16_t device, uint16_t function) {
    pci_device_descriptor dev_des;
    dev_des.bus = bus;
    dev_des.device = device;
    dev_des.function = function;

    dev_des.vendor_id = pci_read(bus, device, function, 0x00);
    dev_des.device_id = pci_read(bus, device, function, 0x02);

    dev_des.class_id = pci_read(bus, device, function, 0x0B);
    dev_des.subclass_id = pci_read(bus, device, function, 0x0A);
    dev_des.interface_id = pci_read(bus, device, function, 0x09);

    dev_des.revision = pci_read(bus, device, function, 0x08);
    dev_des.interrupt = pci_read(bus, device, function, 0x3C);

    return dev_des;
}

base_address_register get_base_address_register(uint16_t bus, uint16_t device, uint16_t function, uint16_t barNum) {

    base_address_register bar;
    uint32_t header_type = pci_read(bus, device, function, 0x0E) & 0x7F;
    int max_bars = 6 - (4 * header_type);
    if (barNum >= max_bars) {
        return bar;
    }
    uint32_t bar_value = pci_read(bus, device, function, 0x10 + 4 * barNum);
    bar.type = (bar_value & 0x1)? INPUT_OUTPUT : MEMORY_MAPPING;
    if (bar.type == MEMORY_MAPPING) {
        switch ((bar_value >> 1) & 0x3) {
            case 0x0: // 32Bit Memory-BAR
            case 0x1: // 20Bit Memory-BAR 
            case 0x2: // 64Bit Memory-BAR
                break;
        }
    } else { // InputOutput Device
        bar.address = (uint8_t *)(bar_value & ~0x3); // Removing the last 2 bits
        bar.prefetchable = ((bar_value >> 3) & 0x1) == 0x1; 
    }
    return bar;
}

void load_pci_drivers() {
    print("Displaying all PCI devices:\n");
    for (int bus = 0; bus < 8; bus++) {  // PCI has 8 bus
        for (int device = 0; device < 32; device++) { // has 32 devices
            int num_functions = device_has_functions(bus, device)? 8 : 1;
            for(int function = 0; function < num_functions; function++) {
                pci_device_descriptor dev_des = get_device_descriptor(bus, device, function);
                if (dev_des.vendor_id == 0x0000 || dev_des.vendor_id == 0xFFFF) {
                    continue;
                }
                for (int barNum = 0; barNum < 6; barNum++) {
                    base_address_register bar = get_base_address_register(bus, device, function, barNum);
                    if (bar.address && bar.type == INPUT_OUTPUT) {
                        dev_des.port_base = (uint32_t) bar.address;
                    }
                }

                print("PCI BUS ");
                print_hex(bus & 0xFF);
                print(", DEVICE ");
                print_hex(device & 0xFF);
                print(", FUNCTION ");
                print_hex(function & 0xFF);
                print(" = VENDOR ");
                print_hex((dev_des.vendor_id & 0xFF00) >> 8);
                print_hex(dev_des.vendor_id & 0xFF);
                print(", DEVICE ");
                print_hex((dev_des.device_id & 0xFF00) >> 8);
                print_hex(dev_des.device_id & 0xFF);
                print(" FOUND\n");
            }
        }
    }
}
