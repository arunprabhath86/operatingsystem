#include <driver/vga.h>
#include <hardware/port.h>

#define MISC_PORT                       0x3C2
#define CRTC_INDEX_PORT                 0x3D4
#define CRTC_DATA_PORT                  0x3D5
#define SEQUENCER_INDEX_PORT            0x3C4
#define SEQUENCER_DATA_PORT             0x3C5
#define GRAPHICS_CONTROLLER_INDEX_PORT  0x3CE
#define GRAPHICS_CONTROLLER_DATA_PORT   0x3CF
#define ATTRIBUTE_CONTROLLER_INDEX_PORT 0x3C0
#define ATTRIBUTE_CONTROLLER_READ_PORT  0x3C1
#define ATTRIBUTE_CONTROLLER_WRITE_PORT 0x3C0
#define ATTRIBUTE_CONTROLLER_RESET_PORT 0x3DA


void put_pixel_internal(uint32_t x, uint32_t y , uint8_t color_index);
uint8_t get_color_index(uint8_t r, uint8_t g, uint8_t b);
void write_registers(uint8_t *reg);
uint8_t get_frame_buffer_segment();


void put_pixel_internal(uint32_t x, uint32_t y , uint8_t color_index) {
    if(x < 0 || 320 <= x || y < 0 || 200 <= y) {
        return;
    }  
    uint8_t *pixel = get_frame_buffer_segment() + 320 * y + x;
    *pixel = color_index;
}

/**
 * 
 */
void vga_put_pixel(uint32_t x, uint32_t y, uint8_t red, uint8_t green, uint8_t blue) {
    put_pixel_internal(x, y, get_color_index(red, green, blue));
}

/**
 * This function checks if the specified mode is supported or not
 * 
 */
int8_t supports_mode(uint32_t width, uint32_t height, uint32_t color_depth) {
    // Currently we support 320x200 and 8 bit color 
    return width == 320 && height == 200 && color_depth == 8;
}

/**
 * this function writes the argument into corresponding 
 * ports.
 **/
void write_registers(uint8_t *regs) {
    unsigned i;

    /* Write MISCELLANEOUS regs*/
    w8_port(MISC_PORT, *regs);
    regs++;

    /* write SEQUENCER regs */
    for(i = 0; i < 5; i++) {
        w8_port(SEQUENCER_INDEX_PORT, i);
        w8_port(SEQUENCER_DATA_PORT, *regs);
        regs++;
    }

    /* write CRTC regs */
    // This will unlock the CRTC controller
    w8_port(CRTC_INDEX_PORT, 0x03);
    w8_port(CRTC_DATA_PORT, r8_port(CRTC_DATA_PORT) | 0x80); // Setting first bit to 1.
    w8_port(CRTC_INDEX_PORT, 0x11);
    w8_port(CRTC_DATA_PORT, r8_port(CRTC_DATA_PORT) & ~0x80); //setting first bit to 0.

    regs[0x03] = regs[0x03] | 0x80;
    regs[0x11] = regs[0x11] & ~0x80;

    for(i = 0; i < 25; i++) {
        w8_port(CRTC_INDEX_PORT, i);
        w8_port(CRTC_DATA_PORT, *regs);
        regs++;
    }

    /* write GraphicController regs */
    for(i = 0; i < 9; i++) {
        w8_port(GRAPHICS_CONTROLLER_INDEX_PORT, i);
        w8_port(GRAPHICS_CONTROLLER_DATA_PORT, *regs);
        regs++;
    }

    /* write Attribute Controller regs */
    for(i = 0; i < 21; i++) {
        r8_port(ATTRIBUTE_CONTROLLER_RESET_PORT);
        w8_port(ATTRIBUTE_CONTROLLER_INDEX_PORT, i);
        w8_port(ATTRIBUTE_CONTROLLER_WRITE_PORT, *regs);
        regs++;
    }
    r8_port(ATTRIBUTE_CONTROLLER_RESET_PORT);
    w8_port(ATTRIBUTE_CONTROLLER_INDEX_PORT, 0x20);
}

/**
 * Taken from https://files.osdev.org/mirrors/geezer/osd/graphics/modes.c
 * */
int8_t vga_set_mode(uint32_t width, uint32_t height, uint32_t color_depth) {
    if (!supports_mode(width, height, color_depth)) {
        return 0;
    }
    unsigned char g_320x200x256[] = {
        /* MISC */
            0x63,
        /* SEQ */
            0x03, 0x01, 0x0F, 0x00, 0x0E,
        /* CRTC */
            0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F,
            0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x9C, 0x0E, 0x8F, 0x28,	0x40, 0x96, 0xB9, 0xA3,
            0xFF,
        /* GC */
            0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F,
            0xFF,
        /* AC */
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
            0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
            0x41, 0x00, 0x0F, 0x00,	0x00
    };
    write_registers(g_320x200x256);
    return 1;
}

uint8_t get_color_index(uint8_t r, uint8_t g, uint8_t b) {
    if(r == 0x00 && g == 0x00 && b == 0x00) 
        return 0x00; // black
    if(r == 0x00 && g == 0x00 && b == 0xA8) 
        return 0x01; // blue
    if(r == 0x00 && g == 0xA8 && b == 0x00) 
        return 0x02; // green
    if(r == 0xA8 && g == 0x00 && b == 0x00) 
        return 0x04; // red
    if(r == 0xFF && g == 0xFF && b == 0xFF) 
        return 0x3F; // white
    return 0x00;
}

uint8_t get_frame_buffer_segment() {
    w8_port(GRAPHICS_CONTROLLER_INDEX_PORT, 0x06);
    uint8_t segment_num = ((r8_port(GRAPHICS_CONTROLLER_DATA_PORT) >> 2) & 0x03);
    switch(segment_num) {
        default:
        case 0: return (uint8_t*)0x00000;
        case 1: return (uint8_t*)0xA0000;
        case 2: return (uint8_t*)0xB0000;
        case 3: return (uint8_t*)0xB8000;
    }
}

void draw_rectangle() {
    for (int32_t y = 0; y < 200; y++) {
         for (int32_t x = 0; x < 320; x++) {
            vga_put_pixel(x, y, 0x00, 0x00, 0xA8);
         }
      }
}





 