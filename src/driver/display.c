#include <driver/display.h>
#include <common/types.h>
#include <hardware/port.h>
#include <util/util.h>

int get_screen_offset(int col, int row);
int get_cursor();
void set_cursor(int offset);
int handle_scrolling(int cursor_offset);

void print_hex(uint8_t key) {
    char* foo = "00";
    char* hex = "0123456789ABCDEF";
    foo[0] = hex[(key >> 4) & 0xF];
    foo[1] = hex[key & 0xF];
    print(foo);
}
void print_hex16(uint16_t key) {
    print_hex((key >> 8) & 0xFF);
    print_hex( key & 0xFF);
}
void print_hex32(uint32_t key) {
    print_hex((key >> 24) & 0xFF);
    print_hex((key >> 16) & 0xFF);
    print_hex((key >> 8) & 0xFF);
    print_hex(key & 0xFF);
}


void print_char(char character, int col, int row, char attribute_byte) {
    unsigned char *vidmem = (unsigned char *) VIDEO_MEMORY;
    if (!attribute_byte) {
        attribute_byte = WHITE_ON_BLACK;
    }
    int offset;
    if (col >= 0 && row >= 0) {
        offset = get_screen_offset(col, row);
    } else {
        offset = get_cursor();
    }
    if (character == '\n') {
        int rows = offset / (2*MAX_COLS);
        offset = get_screen_offset(79, rows);

    } else {
        vidmem[offset] = character;
        vidmem[offset+1] = attribute_byte;
    }
    offset += 2;
    offset = handle_scrolling(offset);
    set_cursor(offset);
}

int get_screen_offset(int col, int row) {
    return ((row * MAX_COLS) + col) * 2;
}

int get_cursor() {
    w8_port(DISPLAY_CTRL_PORT, 14);
    int offset = r8_port(DISPLAY_DATA_PORT) << 8;
    w8_port(DISPLAY_CTRL_PORT, 15);
    offset += r8_port(DISPLAY_DATA_PORT);
    return offset*2;
}

void set_cursor(int offset) {
    offset /= 2;
    w8_port(DISPLAY_CTRL_PORT, 14);
    w8_port(DISPLAY_DATA_PORT, (unsigned char)(offset >> 8));
    w8_port(DISPLAY_CTRL_PORT, 15);
    w8_port(DISPLAY_DATA_PORT, offset);
}

void print_at(string message, int col, int row) {
    if (col >= 0 && row >= 0) {
        set_cursor(get_screen_offset(col, row));
    }
    int i = 0;
    while (message[i] != 0) {
        print_char(message[i++], col, row, WHITE_ON_BLACK);
    }
}

void print(string message) {
    print_at(message, -1, -1);
}

void clear_screen() {
    int row = 0;
    int col = 0;

    for (row=0; row<MAX_ROWS; row++) {
        for (col=0; col<MAX_COLS; col++) {
            print_char(' ', col, row, WHITE_ON_BLACK);
        }
    }
    set_cursor(get_screen_offset(0, 0));
}


int handle_scrolling(int cursor_offset) {
    if (cursor_offset < MAX_ROWS*MAX_COLS*2) {
        return cursor_offset;
    }
    int i;
    for (i=1; i<MAX_ROWS; i++) {
        memory_copy((char *)(uint32_t)get_screen_offset(0, i) + VIDEO_MEMORY,
                    (char *)(uint32_t)get_screen_offset(0, i-1) + VIDEO_MEMORY,
                    MAX_COLS * 2
        );
    }
    char* last_line = (char *)(uint32_t)get_screen_offset(0, MAX_ROWS-1) + VIDEO_MEMORY;
    for (i=0; i<MAX_COLS*2; i++) {
        last_line[i] = 0;
    }
    cursor_offset -= 2*MAX_COLS;
    return cursor_offset;
}
