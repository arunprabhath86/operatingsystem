#include <driver/keyboard.h>
#include <driver/display.h>
#include <common/types.h>
#include <interrupt/isr.h>
#include <hardware/port.h>

static int shift = 0;

static void handle_keyboard_interrupt(registers_t regs) {
    uint8_t key = r8_port(KEYBOATD_DATA_PORT);
    
        switch (key) {
            // print should be replaced with a call back function like 
            // onKeypress or similar
            case 0xFA: 
            case 0x45: 
            case 0xC5: break;
            case 0x1E: if (shift) print("A"); else print("a"); break;
            case 0x30: if (shift) print("B"); else print("b"); break;
            case 0x2E: if (shift) print("C"); else print("c"); break;
            case 0x20: if (shift) print("D"); else print("d"); break;
            case 0x12: if (shift) print("E"); else print("e"); break;
            case 0x21: if (shift) print("F"); else print("f"); break;
            case 0x22: if (shift) print("G"); else print("g"); break;
            case 0x23: if (shift) print("H"); else print("h"); break;
            case 0x17: if (shift) print("I"); else print("i"); break;
            case 0x24: if (shift) print("J"); else print("j"); break;
            case 0x25: if (shift) print("K"); else print("k"); break;
            case 0x26: if (shift) print("L"); else print("l"); break;
            case 0x32: if (shift) print("M"); else print("m"); break;
            case 0x31: if (shift) print("N"); else print("n"); break;
            case 0x18: if (shift) print("O"); else print("o"); break;
            case 0x19: if (shift) print("P"); else print("p"); break;
            case 0x10: if (shift) print("Q"); else print("q"); break;
            case 0x13: if (shift) print("R"); else print("r"); break;
            case 0x1F: if (shift) print("S"); else print("s"); break;
            case 0x14: if (shift) print("T"); else print("t"); break;
            case 0x16: if (shift) print("U"); else print("u"); break;
            case 0x2F: if (shift) print("V"); else print("v"); break;
            case 0x11: if (shift) print("W"); else print("w"); break;
            case 0x2D: if (shift) print("X"); else print("x"); break;
            case 0x15: if (shift) print("Y"); else print("y"); break;
            case 0x2C: if (shift) print("Z"); else print("z"); break;
            case 0x39: print(" "); break;
            case 0x1C: print("\n");break;
            case 0x33: if (shift) print("<"); else print(","); break;
            case 0x34: if (shift) print(">"); else print("."); break;
            case 0x35: if (shift) print("_"); else print("-"); break;
            case 0x02: if (shift) print("!"); else print("1"); break;
            case 0x03: if (shift) print("@"); else print("2"); break;
            case 0x04: if (shift) print("#"); else print("3"); break;
            case 0x05: if (shift) print("$"); else print("4"); break;
            case 0x06: if (shift) print("%"); else print("5"); break;
            case 0x07: if (shift) print("^"); else print("6"); break;
            case 0x08: if (shift) print("&"); else print("7"); break;
            case 0x09: if (shift) print("*"); else print("8"); break;
            case 0x0A: if (shift) print("("); else print("9"); break;
            case 0x0B: if (shift) print(")"); else print("0"); break;
            case 0x2A:
            case 0x36: shift = 1; break;
            case 0xAA:
            case 0xB6: shift = 0; break;
            default: 
                if (key < 0x80) {
                    print("KEYBOARD: ");
                    print_hex(key);
                    print("\n");
                }
                break;

        }
    
    
}


void init_keyboard_driver() {

    while(r8_port(KEYBOARD_COMMAND_PORT) & 0x1) {
        r8_port(KEYBOATD_DATA_PORT);
    }
    // activate interrupts
    w8_port(KEYBOARD_COMMAND_PORT, 0xAE);
    w8_port(KEYBOARD_COMMAND_PORT, 0x20);

    uint8_t status = (r8_port(KEYBOATD_DATA_PORT) | 1) & ~0x10;
    w8_port(KEYBOARD_COMMAND_PORT, 0x60);
    w8_port(KEYBOATD_DATA_PORT, status);

    w8_port(KEYBOATD_DATA_PORT, 0xF4);


   //Registering keyboard interrupt callback function.
   register_interrupt_handler(IRQ1, &handle_keyboard_interrupt);
   print("Keyboard driver initialised. Keyboard is active\n");

}