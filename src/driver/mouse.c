#include <driver/mouse.h>
#include <driver/display.h>
#include <interrupt/isr.h>
#include <hardware/port.h>
#include <common/types.h>

uint8_t mouse_buffer[3];
uint8_t mouse_offset;
uint8_t mouse_buttons;
static int8_t x = 40, y = 12;

void flip_cursor(int8_t x, int8_t y);

/**
 * This is the mouse interrupt handler function which 
 * is invoked by the interrupt handler.
 * */
static void handle_mouse_interrupt(registers_t regs) {
   uint8_t status = r8_port(MOUSE_COMMAND_PORT);
   if (!(status & 0x20)) {
       return;
   }
   mouse_buffer[mouse_offset] = r8_port(MOUSE_DATA_PORT);
   mouse_offset = (mouse_offset + 1) % 3;
   if (mouse_offset == 0) {
        // Flipping the old cursor point back to normal
        flip_cursor(x, y);
        x = x + mouse_buffer[1];
        if (x < 0) {
            x = 0;
        }
        if (x >= 80) {
            x = 79;
        }
        y = y - mouse_buffer[2];
        if (y < 0) {
            y = 0;
        }
        if (y >= 25) {
            y = 24;
        }
        // Flipping the color of the cursor point
        flip_cursor(x, y);
        for (uint8_t i = 0; i < 3; i++) {
            if((mouse_buffer[0] & (0x1 << i)) != (mouse_buttons & (0x1 << i))) {
                // here is the click happens 
                // currently it flips the color of the current location
                // later can be changed to click handler or something similar
                flip_cursor(x, y);
            }
        }
        mouse_buttons = mouse_buffer[0];
   }
}

/**
 * This function flips the background color with foreground color 
 * and viceversa. Here each character is represented by 2 bytes
 * 
 *  FGCL BGCL [8bit char]
 *  |----|----|----|----| -- 16 bit in total
 * 
 *  Replacing FGCL(foreground color) with BGCL( Background color)
 *  Replacing BGCL with FGCL 
 *  And dont change the last 8 bit (1 byte) whic is the actual 
 *  character
 * */
void flip_cursor(int8_t x, int8_t y) {
    static uint16_t *videoMem = (uint16_t *)VIDEO_MEMORY;
    videoMem[80 * y + x] = ((videoMem[80 * y + x] & 0xF000) >> 4 |
                       (videoMem[80 * y + x] & 0x0F00) << 4 |
                       (videoMem[80 * y + x] & 0x00FF));
    
}


void init_mouse_driver() {

    mouse_offset = 0;
    mouse_buttons = 0;

    // Setting the cursor to middle of the screen
    flip_cursor(x, y);

    // Setting up the mouse 
    w8_port(MOUSE_COMMAND_PORT, 0xA8);
    w8_port(MOUSE_COMMAND_PORT, 0x20);
    uint8_t status = r8_port(MOUSE_DATA_PORT) | 2;
    w8_port(MOUSE_COMMAND_PORT, 0x60);
    w8_port(MOUSE_DATA_PORT, status);

    w8_port(MOUSE_COMMAND_PORT, 0xD4);
    w8_port(MOUSE_DATA_PORT, 0xF4);
    r8_port(MOUSE_DATA_PORT);

    register_interrupt_handler(IRQ12, &handle_mouse_interrupt);
    print("Mouse driver initialised. Mouse is active.\n");
}