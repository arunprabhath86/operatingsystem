
#include <common/types.h>
#include <driver/display.h>
#include <driver/keyboard.h>
#include <driver/mouse.h>
#include <interrupt/interrupt.h>
#include <gdt.h>
#include <driver/timer.h>
#include <hardware/pci.h>
#include <driver/vga.h>


void kernel_init(void *multiboot, unsigned int magicNumber) {
   print("________WELCOME TO OUR OPERATING STSTEM___________\n");

   gdt_entry_t *gdt = (gdt_entry_t *)init_global_descriptor_table();
   init_interrupt_descriptor_tables();
   init_keyboard_driver();
   init_mouse_driver();
   load_pci_drivers();
   
   enable_interrupt();
   /*
      This should be deleted. this is for just testing 
      vga_set_mode(320, 200, 8);
      draw_rectangle();
   **/
  
  
   while(1);
}
