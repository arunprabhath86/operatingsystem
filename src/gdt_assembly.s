

#Call this function to load gdt from our c code.
.section .text
.global gdt_flush


gdt_flush:
    mov 4(%esp), %eax
    lgdt (%eax)
    mov  $0x10, %ax
    mov  %ax, %ds
    mov  %ax, %es
    mov  %ax, %fs
    mov  %ax, %gs
    mov  %ax, %ss
    jmp $0x08, $flush

flush:
    ret

#Call this function to load idt from our c code.
.global idt_flush

idt_flush:
    mov 4(%esp), %eax
    lidt (%eax)
    ret


    