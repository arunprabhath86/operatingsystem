#include <interrupt/isr.h>
#include <driver/display.h>
#include <hardware/port.h>


isr_t interrupt_handlers[256];


void register_interrupt_handler(uint8_t n, isr_t handler) {
  interrupt_handlers[n] = handler;
}

// This gets called from our assembly interrupt handler stub.
void isr_handler(registers_t regs) {
   print("Recieved interrupt: ");
   print_hex32(regs.int_no);
   print("\n");
}

// This gets called from our assembler interrupt handler stub.
void irq_handler(registers_t regs) {
   
   // Calling registered interrupt handlers.
   if (interrupt_handlers[regs.int_no] != 0) {
       isr_t handler = interrupt_handlers[regs.int_no];
       handler(regs);
   } else if (regs.int_no != 0x20) {
      print("Unhandled interrupt: ");
      print_hex16(regs.int_no);
      print("\n");
   }

   /**
    * Handling only hardware interrupts that ranges between
    * inclusive 0x20 and 0x30
    * */
   if (0x20 <= regs.int_no && regs.int_no < 0x30) {
      //Send master pic command port EOI
      w8_port_slow(PIC_MASTER_COMMAND, 0x20);
      if (0x28 <= regs.int_no) {
         //Send slave pic command port EOI
         w8_port_slow(PIC_SLAVE_COMMAND, 0x20);
      }
   }
   
}