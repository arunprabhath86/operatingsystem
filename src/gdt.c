
#include <gdt.h>
#include <driver/display.h>
#include <util/util.h>

// This function is defined in the assembly code (gdt_assembly.s)
// This functin to load the GDT pointer into the GDT registers 
// in the processor.
extern void gdt_flush(uint32_t);

// Internal function prototypes.
static void init_gdt();
static void gdt_set_gate(int32_t, uint32_t, uint32_t, uint8_t);

gdt_entry_t gdt_entries[5];
gdt_ptr_t   gdt_ptr;

// initialises the GDT.
uint32_t init_global_descriptor_table() {
   // Initialise the global descriptor table.
   init_gdt();
   return gdt_ptr.base;
}
static void init_gdt() {
   gdt_ptr.limit = (sizeof(gdt_entry_t) * 5) - 1;
   gdt_ptr.base  = (uint32_t)&gdt_entries;

   gdt_set_gate(0, 0, 0, 0);               // Null segment
   gdt_set_gate(1, 0, 64*1024*1024, 0x9A); // Code segment - Limit is 64MB
   gdt_set_gate(2, 0, 64*1024*1024, 0x92); // Data segment - Limit is 64MB
   gdt_set_gate(3, 0, 64*1024*1024, 0xFA); // User mode code segment - Limit is 64MB
   gdt_set_gate(4, 0, 64*1024*1024, 0xF2); // User mode data segment - Limit is 64MB

   //Calling assembly function to load gdt entry
   gdt_flush((uint32_t)&gdt_ptr);
   print("Gloabl descriptor table is loaded\n");
}

// Set the value of one GDT entry.
static void gdt_set_gate(int32_t num, uint32_t base, uint32_t limit, uint8_t type) {

    gdt_entry_t *entry = &(gdt_entries[num]);
    uint8_t* target = (uint8_t*) (entry);

    if (limit <= 65536) {
        // 16-bit address space
        target[6] = 0x40;
    } else {
        if((limit & 0xFFF) != 0xFFF)
            limit = (limit >> 12)-1;
        else
            limit = limit >> 12;

        target[6] = 0xC0;
    }

    // Encode the limit
    target[0] = limit & 0xFF;
    target[1] = (limit >> 8) & 0xFF;
    target[6] |= (limit >> 16) & 0xF;

    // Encode the base
    target[2] = base & 0xFF;
    target[3] = (base >> 8) & 0xFF;
    target[4] = (base >> 16) & 0xFF;
    target[7] = (base >> 24) & 0xFF;

    // Type
    target[5] = type;
}
