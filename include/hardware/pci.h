#ifndef __HARDWARE_PCI_H__
#define __HARDWARE_PCI_H__

#include <common/types.h>

/**
 * This code is completely based on the following website. PCI headers and other information
 * are avaiable on 
 * http://www.lowlevel.eu/wiki/Peripheral_Component_Interconnect#Konfigurations-Adressraum
 * 
 * */

#define PCI_DATA_PORT 0xCFC
#define PCI_COMMAND_PORT 0xCF8

/**
 * 
 * */
typedef struct  {
    uint32_t port_base;
    uint32_t interrupt;

    uint16_t bus;
    uint16_t device;
    uint16_t function;

    uint16_t vendor_id;
    uint16_t device_id;

    uint8_t class_id;
    uint8_t subclass_id;
    uint8_t interface_id;
    uint8_t revision;
} pci_device_descriptor;

enum base_address_register_type {
    MEMORY_MAPPING = 0,
    INPUT_OUTPUT = 1
};

typedef struct {
    int8_t prefetchable;
    uint8_t *address;
    uint32_t size;
    int8_t type;
} base_address_register;

uint32_t pci_read(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset);
void pci_write(uint16_t bus, uint16_t device, uint16_t function, uint32_t register_offset, uint32_t data);
int8_t device_has_functions(uint16_t bus, uint16_t device);
pci_device_descriptor get_device_descriptor(uint16_t bus, uint16_t device, uint16_t function);
void load_pci_drivers();
base_address_register get_base_address_register(uint16_t bus, uint16_t device, uint16_t function, uint16_t barNum);

#endif