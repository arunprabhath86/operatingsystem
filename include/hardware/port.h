#ifndef __HARDWARE_PORT_H__
#define __HARDWARE_PORT_H__

#include <common/types.h>

#define PIC_MASTER		0x20		/* IO base address for master PIC */
#define PIC_SLAVE		0xA0		/* IO base address for slave PIC */
#define PIC_MASTER_COMMAND	PIC_MASTER
#define PIC_MASTER_DATA	(PIC_MASTER+1)
#define PIC_SLAVE_COMMAND	PIC_SLAVE
#define PIC_SLAVE_DATA	(PIC_SLAVE+1)

// Screen device I/O ports
#define DISPLAY_CTRL_PORT 0x3D4
#define DISPLAY_DATA_PORT 0x3D5

// keyboard device ports
#define KEYBOARD_COMMAND_PORT 0x64
#define KEYBOATD_DATA_PORT 0x60

//mouse device ports - same as keyboard
#define MOUSE_COMMAND_PORT 0x64
#define MOUSE_DATA_PORT 0x60

static inline void w8_port(uint16_t port, uint8_t _data) {
    __asm__ volatile("outb %0, %1" : : "a" (_data), "Nd" (port));
}
static inline uint8_t r8_port(uint16_t port){
    uint8_t result;
    __asm__ volatile("inb %1, %0" : "=a" (result) : "Nd" (port));
    return result;
}

static inline void w8_port_slow(uint16_t port, uint8_t _data) {
    __asm__ volatile("outb %0, %1\njmp 1f\n1: jmp 1f\n1:" : : "a" (_data), "Nd" (port));
}


static inline void w16_port(uint16_t port, uint16_t _data){
    __asm__ volatile("outw %0, %1" : : "a" (_data), "Nd" (port));
}
static inline uint16_t r16_port(uint16_t port){
    uint16_t result;
    __asm__ volatile("inw %1, %0" : "=a" (result) : "Nd" (port));
    return result;
}

static inline void w32_port(uint16_t port, uint32_t _data){
    __asm__ volatile("outl %0, %1" : : "a"(_data), "Nd" (port));
}
static inline uint32_t r32_port(uint16_t port){
    uint32_t result;
    __asm__ volatile("inl %1, %0" : "=a" (result) : "Nd" (port));
    return result;
}

#endif