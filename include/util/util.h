#ifndef __OS_UTIL_H__
#define __OS_UTIL_H__

#include <common/types.h>

void memory_copy(char* source, char* dest, int no_bytes);
void memset(uint8_t *dest, uint8_t val, uint32_t len);

#endif