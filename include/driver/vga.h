#ifndef __DRIVER_VGA_H__
#define __DRIVER_VGA_H__

#include <common/types.h>

int8_t supports_mode(uint32_t width, uint32_t height, uint32_t color_depth);

int8_t vga_set_mode(uint32_t width, uint32_t height, uint32_t color_depth);

void vga_put_pixel(uint32_t x, uint32_t y, uint8_t red, uint8_t green, uint8_t blue);


void draw_rectangle();

#endif