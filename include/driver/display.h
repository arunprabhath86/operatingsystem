#ifndef __DEVICE_DISPLAY_H__
#define __DEVICE_DISPLAY_H__

#include <common/types.h>

//Color video memory 0x000B8000 - 0x000BFFFF 
#define VIDEO_MEMORY 0xB8000 
#define MAX_ROWS 25
#define MAX_COLS 80
// Default colour scheme .
#define WHITE_ON_BLACK 0x0f


/**
 * This function print ascii charecters to display.
 **/
void print(string message);

/**
 * This function will clear the current display 
 * and the cursor will be reset to [0,0]
 **/
void clear_screen();
/**
 * This function prints the 8 bit character into
 * hex format.  
 **/
void print_hex(uint8_t key);
/**
 * This function prints the 16 bit character into
 * hex format.  
 **/
void print_hex16(uint16_t key);
/**
 * This function prints the 32 bit character into
 * hex format.  
 **/
void print_hex32(uint32_t key);

#endif