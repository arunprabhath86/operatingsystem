#ifndef __TIMER_H__
#define __TIMER_H__

#include <common/types.h>

void init_timer(uint32_t frequency);

#endif